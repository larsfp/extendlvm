#!/bin/bash

# GPL 2021, <dev@falkp.no>

echo "Extend LVM to new disk
Will look for new, empty block devices, create one LVM partition, and extend the first LVM VG onto it.
Arguments are optional."


set -o pipefail
set -u # stop on empty vars

if [ $# -gt 0 ]; then
    newdisk="$1"
else
    newdisk=/dev/$(lsblk | tail -n1 | cut -f 1 -d' ')
fi
echo "Using disk <$newdisk>"

test -b "$newdisk" || {
    echo "Disk given is not a block device."
    exit 1;
}

fdisk -l "$newdisk" | /bin/grep "Disklabel" && {
    echo "Disk given already has a partition table."
    exit 1;
}

echo "Hit enter to start non-reversible actions, or ctrl+c to abort."
read -r

echo "Creating a new partition table and LVM type partition"
(
echo g # Create a new empty partition table
echo n # Add a new partition
echo p # Primary partition
echo   # Partition number (Accept default: 1)
echo   # First sector (Accept default)
echo   # Last sector (Accept default)
echo t # Change type
echo 31 # LVM
echo w # Write changes
) | fdisk "${newdisk}"

echo "Creating a PV"
pvcreate "${newdisk}1"

vgname=$(vgs --noheadings --rows | head -n1 | tr -d ' ')
echo "Will extend VG <$vgname>"

vgextend "$vgname" "${newdisk}1"

echo "Will extend LV named root"
lvextend -l +100%FREE "/dev/$vgname/root"
resize2fs "/dev/$vgname/root"

test -f /lib/systemd/system/xe-daemon.service && {
    echo "xe-daemon often crash after extending disks. Restarting it."
    systemctl restart xe-daemon;
}

echo "Done"
